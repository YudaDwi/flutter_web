import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class LandingPage extends StatefulWidget {
  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    //7 buat widget navItem
    //jadi dibuat title untuk dicustom judulnya
    //untuk index jadi kita bisa tau nanti posisinya ada dimana
    Widget navItem({String title, int index}) {
      return InkWell(
        onTap: () {
          setState(() {
            selectedIndex = index;
          });
        },
        child: Column(
          mainAxisSize: MainAxisSize.min,
          //artinya columnya setinggi dengan childrenya
          children: [
            Text(title,
                style: GoogleFonts.poppins(
                    fontSize: 18,
                    color: Color(0xff1D1E3C),
                    fontWeight: index == selectedIndex
                        ? FontWeight.w500
                        : FontWeight.w300)),
            Container(
              width: 66,
              height: 2,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: index == selectedIndex
                    ? Color(0xffFE998D)
                    : Colors.transparent,
              ),
            )
          ],
        ),
      );
    }

    return Scaffold(
        body: Container(
      child: ListView(
        children: [
          Stack(
            children: [
              Image.asset(
                'background.png',
                width: MediaQuery.of(context).size.width,
                //artiya ukuran lebar disesuaikan dengan ukuran lebar default/aslinya
                height: MediaQuery.of(context).size.height,
                fit: BoxFit.fill,
                //fungsi boxfit fil fungsinya agar bisa menyesuaikan properti dari width dan height
              ),
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 100, vertical: 30),
                  child: Column(
                    children: [
                      Wrap(
                        children: [
                          Row(
                            //NOTE: HEADER == Row
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Image.asset(
                                'logo.png',
                                width: 72,
                                height: 40,
                              ),
                              Row(children: [
                                navItem(title: 'Guides', index: 0),
                                SizedBox(
                                  width: 44,
                                ),
                                navItem(title: 'Pricing', index: 1),
                                SizedBox(
                                  width: 44,
                                ),
                                navItem(title: 'Team', index: 2),
                                SizedBox(
                                  width: 44,
                                ),
                                navItem(title: 'Stories', index: 3),
                              ]),
                              Image.asset(
                                'button_account.png',
                                width: 163,
                                height: 53,
                              )
                            ],
                          ),
                        ],
                      ),
                      //NOTE: Content
                      SizedBox(
                        height: 76,
                      ),
                      Image.asset(
                        'illustration.png',
                        height: 550,
                        width: 763,
                      ),
                      //NOTE: Footer
                      SizedBox(
                        height: 84,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset(
                            'icon_scroll.png',
                            width: 24,
                          ),
                          SizedBox(
                            width: 13,
                          ),
                          Text(
                            'Scroll To Learn More',
                            style: GoogleFonts.poppins(
                                fontSize: 20, color: Colors.black),
                          )
                        ],
                      )
                    ],
                  ))
            ],
          ),
        ],
      ),
    ));
  }
}
